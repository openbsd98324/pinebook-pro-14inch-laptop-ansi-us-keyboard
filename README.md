
````
14″ PINEBOOK Pro LINUX LAPTOP (ANSI. US Keyboard)
Community price: $219.99
(Retail price: $299.99)

Pinebook Pro orders will dispatch weekly from Hong Kong 3PL (Third Party Logistic) warehouse.
CPU: 64-Bit Dual-Core ARM 1.8GHz Cortex A72 and Quad-Core ARM 1.4GHz Cortex A53
GPU: Quad-Core MALI T-860
RAM: 4 GB LPDDR4 Dual Channel System DRAM Memory
Flash: 64 GB eMMC 5.0
Wireless: WiFi 802.11AC + Bluetooth 5.0
One USB 3.0 and one USB 2.0 Type-A Host Ports
USB 3.0 Type-C ports with alt-mode display out (DP 1.2) and 15W 5V 3A charge.
MicroSD Card Slot: 1
Headphone Jack: 1
Microphone: Built-in
Keyboard: Full Size ANSI(US) type Keyboard
Touch-pad: Large Multi-Touch Touchpad
Power: Input: 100~240V, Output: 5V3A
Battery: Lithium Polymer Battery (9600mAH)
Display: 14.1″ IPS LCD (1920 x 1080)
Front Camera: 2.0 Megapixels
Power Supply included, comes with both US and EU plugs
Dimension: 329mm x 220mm x 12mm (WxDxH)
Weight: 1.26 kg (2.78 lbs)
Warranty: 30 days
````


![](pub/medias/PBP-ANSI-Keyboard-layout.png)
![](pub/medias/PBPfront.jpg)
![](pub/medias/PBPleft.jpg)
![](pub/medias/PBPright-1.jpg)

# Software (Linux)

Anyone with a PINE64 forum account (forums.pine64.org) can contribute to the Wiki.      

Getting Started: Your Pinebook Pro ships with Manjaro, one of the most popular and innovative desktop Linux operating systems available. Manjaro is also host to a large, vibrant and friendly community of end-users like yourself. There is even a dedicated ARM-device subsection on their forum (forum.manjaro.org/manjaro-arm) featuring frequent updates from developers and contributors. I encourage you to visit Manjaro’s website (manjaro.org) and forums to get acquainted with the operating system and the people behind it. The Manjaro build that ships with the Pinebook Pro features the latest Linux kernel and open source drivers. This extends to the open-source Panfrost GPU driver, which accelerates the KDE Plasma Desktop using OpenGL 2.0. This Manjaro build has support for all hardware features of the Pinebook Pro, including USB-C video out and charging.

First boot: Upon first boot, you will be asked to create a user and password, as well as select your keyboard laybout and locale. We suggest that you do not plug in any external USB peripherals (USB-C docks, SSD/HDDs, mice, etc.,) on the first boot. Once the initial setup is complete the Pinebook Pro will reboot and you’ll be greeted with the log-in screen. 



pinebook pro:  CPU: 64-Bit Dual-Core ARM 1.8GHz Cortex A72 and Quad-Core ARM 1.4GHz Cortex A53 
versus PI4: Raspberry Pi 4 Model B Rev 1.4 Broadcom BCM2711, Quad core Cortex-A72 (ARM v8) 64-bit SoC @ 1.5GHz


# PI4

````
processor	: 0
model name	: ARMv7 Processor rev 3 (v7l)
BogoMIPS	: 108.00
Features	: half thumb fastmult vfp edsp neon vfpv3 tls vfpv4 idiva idivt vfpd32 lpae evtstrm crc32 
CPU implementer	: 0x41
CPU architecture: 7
CPU variant	: 0x0
CPU part	: 0xd08
CPU revision	: 3

processor	: 1
model name	: ARMv7 Processor rev 3 (v7l)
BogoMIPS	: 108.00
Features	: half thumb fastmult vfp edsp neon vfpv3 tls vfpv4 idiva idivt vfpd32 lpae evtstrm crc32 
CPU implementer	: 0x41
CPU architecture: 7
CPU variant	: 0x0
CPU part	: 0xd08
CPU revision	: 3

processor	: 2
model name	: ARMv7 Processor rev 3 (v7l)
BogoMIPS	: 108.00
Features	: half thumb fastmult vfp edsp neon vfpv3 tls vfpv4 idiva idivt vfpd32 lpae evtstrm crc32 
CPU implementer	: 0x41
CPU architecture: 7
CPU variant	: 0x0
CPU part	: 0xd08
CPU revision	: 3

processor	: 3
model name	: ARMv7 Processor rev 3 (v7l)
BogoMIPS	: 108.00
Features	: half thumb fastmult vfp edsp neon vfpv3 tls vfpv4 idiva idivt vfpd32 lpae evtstrm crc32 
CPU implementer	: 0x41
CPU architecture: 7
CPU variant	: 0x0
CPU part	: 0xd08
CPU revision	: 3

Hardware	: BCM2711
Revision	: c03114
Serial		: 1000000004175d94
Model		: Raspberry Pi 4 Model B Rev 1.4

````


